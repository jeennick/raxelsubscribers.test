﻿using G21;
using RaxelPushDataModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RaxelPushService
{
    class Program
    {
        static StreamWriter sw;

        static string addressWebService = string.Empty;
        private static readonly string pushMessage = ConfigurationManager.AppSettings["pushmessage"];
        private static readonly string pushMessageFrodo = ConfigurationManager.AppSettings["pushMessageFrodo"];
        private static readonly string pushCertFileParh = ConfigurationManager.AppSettings["CertificatePath"];
        private static readonly string PushErrorEmailRecipients = ConfigurationManager.AppSettings["PushErrorEmailRecipients"];
        static void Main(string[] args)
        {
            try
            {
                sw = new StreamWriter(ConfigurationManager.AppSettings["logFile"], true);
                Log("Запуск");
                string address;
                int period;
                GetParams(out address, out period);
                var ie = new RaxelPushDataModel.incomingEntities();
                System.Data.Entity.Core.Objects.ObjectResult<GetPushSubscribers_Result> subscribers = null;
                System.Data.Entity.Core.Objects.ObjectResult<GetFrodoPushSubscribers_Result> frodoSubscribers = null;
                while (true)
                {
                    try
                    {
                        subscribers = ie.GetPushSubscribers();
                        frodoSubscribers = ie.GetFrodoPushSubscribers();
                    }
                    catch(Exception ex)
                    {
                        var error = string.Empty;
                        error = string.Format("Причина: {0}", ex);
                        if(!ex.InnerException.ToString().Contains("timeout"))
                            ErrorEmailToAdmin("Raxel Errors. RaxelPushService. Ошибка при запросе пуш-подписчиков", error);
                        Log("Ошибка при запросе пуш-подписчиков. " + error);
                        
                        System.Threading.Thread.Sleep(1000 * period);
                        continue;
                    }
                    //Log("Вызов веб-сервиса для " + subscribers. + " записей");
                    foreach (var s in subscribers)
                    {
                        try
                        {
                            RunPush(s);
                        }
                        catch (Exception e)
                        {
                            var error = string.Empty;
                            error = string.Format("Устройство: {0}; e-mail: {1}; UDID: {2}. Причина: {3}", s.Device_ID, s.email, s.MobileDeviceUID, e);
                            ErrorEmailToAdmin("Raxel Errors. RaxelPushService. Ошибка при отправке пуш-уведомления об аварии", error);
                            Log("Ошибка отправки пуш. " + error);
                        }
                        try
                        {
                            ie.UpdatePushSubscribers(s.DeviceID);
                        }
                        catch (Exception e)
                        {
                            var error = string.Empty;
                            error = string.Format("Устройство: {0}; e-mail: {1}; UDID: {2}. Причина: {3}", s.Device_ID, s.email, s.MobileDeviceUID, e);
                            ErrorEmailToAdmin("Raxel Errors. RaxelPushService. Ошибка при простановке флага WasPushed = 1", error);
                            Log("Ошибка отправки пуш. " + error);
                        }
                    }
                    foreach (var f in frodoSubscribers)
                    {
                        try
                        {
                            RunFrodoPush(f);
                        }
                        catch (Exception e)
                        {
                            var error = string.Empty;
                            error = string.Format("Устройство: {0}; e-mail: {1}; UDID: {2}. Причина: {3}", f.DeviceName, f.email, f.MobileDeviceUID, e);
                            ErrorEmailToAdmin("Raxel Errors. RaxelPushService. Ошибка при отправке пуш-уведомления о выткнутом устройстве", error);
                            Log("Ошибка отправки пуш. " + error);
                        }
                        try
                        {
                            ie.UpdateFrodoPushSubscribers(f.DeviceID);
                        }
                        catch(Exception e)
                        {
                            var error = string.Empty;
                            error = string.Format("Устройство: {0}; e-mail: {1}; UDID: {2}. Причина: {3}", f.DeviceName, f.email, f.MobileDeviceUID, e);
                            ErrorEmailToAdmin("Raxel Errors. RaxelPushService. Ошибка при простановке флага WasPushed = 1 для выткнутого устройства", error);
                            Log("Ошибка отправки пуш. " + error);
                        }
                    }
                    System.Threading.Thread.Sleep(1000 * period);
                }
            }
            catch (Exception e)
            {
                var error = string.Empty;
                error = string.Format("Причина: {0}", e);
                ErrorEmailToAdmin("Raxel Errors. RaxelPushService. Остановка пуш-сервиса!!!", error);
                Log("Остановка пуш-сервиса. " + error);
                Stop();
            }
            Console.Read();
        }

        static void Stop()
        {
            Log("Остановка");
            try { sw.Close(); }
            catch { }
        }

        
        static void Log(string s)
        {
            try
            {
                sw.WriteLine(DateTime.Now.ToString() + "] [" + s);
                sw.Flush();
            }
            catch { }
        }

        static void RunPush(object o)
        {
            GetPushSubscribers_Result res = o as GetPushSubscribers_Result;
            if(res != null && !string.IsNullOrEmpty(res.MobileDeviceUID))
            {
                var mcEnt = new RaxelPushDataModel.MobileClientsEntities();
                try
                {
                    var pns = new PushNotificationCore.PushNotificationSender();
                    pns.SendNotification(res.MobileDeviceUID, pushMessage, AppDomain.CurrentDomain.BaseDirectory + pushCertFileParh);
                    mcEnt.WriteLog(res.email, null, null, null, null, null, "PushNotification", null, Guid.NewGuid().ToString(), "Пушуведомление успешно отправлено", "INFO");
                }
                catch(Exception ex)
                {
                    mcEnt.WriteLog(res.email, null, null, null, null, null, "PushNotification", null, Guid.NewGuid().ToString(), "Пушуведомление отправлено с ошибками: " + ex.Message, "ERROR");
                    throw ex;
                }
            }
        }

        static void RunFrodoPush(object o)
        {
            GetFrodoPushSubscribers_Result res = o as GetFrodoPushSubscribers_Result;
            if (res != null)
            {
                var mcEnt = new RaxelPushDataModel.MobileClientsEntities();
                try
                {
                    //send email to sk about unplagged
                    var client = new RaxelPdfService.UralsibinsNotificatorClient("NotificationService.Uralsibins");
                    var alert11 = new RaxelPdfService.Alert11();
                    alert11.Mark = res.Brand;
                    alert11.Model = res.Model;
                    alert11.Email = res.email;
                    alert11.Fio = res.FIO;
                    alert11.Number = res.DeviceName;
                    alert11.Phone = res.Mobile;
                    alert11.ContractNumber = res.ContractNumber;
                    alert11.Date = res.UnplagDate??DateTime.Now;
                    client.OneWayAlert11Notification(alert11);
                 
                    //send push
                    if (!string.IsNullOrEmpty(res.MobileDeviceUID))
                    {
                        var pns = new PushNotificationCore.PushNotificationSender();
                        pns.SendNotification(res.MobileDeviceUID, pushMessageFrodo, AppDomain.CurrentDomain.BaseDirectory + pushCertFileParh);
                        mcEnt.WriteLog(res.email, null, null, null, null, null, "FrodoPushNotification", null, Guid.NewGuid().ToString(), "Пушуведомление о мошенничестве успешно отправлено", "INFO");
                    }
                }
                catch (Exception ex)
                {
                    mcEnt.WriteLog(res.email, null, null, null, null, null, "FrodoPushNotification", null, Guid.NewGuid().ToString(), "Пушуведомление о мошенничестве отправлено с ошибками: " + ex.Message, "ERROR");
                    throw ex;
                }
            }
        }

        static void ErrorEmailToAdmin(string subject, string message)
        {
            try
            {
                var client = new RaxelEmailService.EmailNotificatorClient();
                var rec = new List<RaxelEmailService.EmailRecipient>();
                var recps = PushErrorEmailRecipients.Split(';');
                foreach(var r in recps)
                {
                    rec.Add(new RaxelEmailService.EmailRecipient() { Email = r.Trim() });
                }
                client.Send(subject, message, rec.ToArray());
            }
            catch(Exception ex)
            {
                Log("Ошибка при отправке почты. " + ex);
            }
        }

        static void GetParams(out string addressWebService, out int period)
        {
            addressWebService = ConfigurationManager.AppSettings["address"];
            period = int.Parse(ConfigurationManager.AppSettings["interval"]);
        }
    }
}
