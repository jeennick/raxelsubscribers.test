﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxelPushModelBLT
{
    public class FrodoPushSubscriber
    {
        public Nullable<int> DeviceID { get; set; }
        public string email { get; set; }
        public string MobileDeviceUID { get; set; }
        public string DeviceName { get; set; }
        public string FIO { get; set; }
        public string Mobile { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string ContractNumber { get; set; }
        public Nullable<System.DateTime> UnplagDate { get; set; }
    }
}
