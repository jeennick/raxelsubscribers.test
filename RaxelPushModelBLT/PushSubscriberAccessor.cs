﻿using BLToolkit.Data;
using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaxelPushModelBLT
{
    public abstract class PushSubscriberAccessor : DataAccessor
    {
        protected override BLToolkit.Data.DbManager CreateDbManager()
        {
            var dbm = new DbManager("SQL", "incomingEntities");
            dbm.Command.CommandTimeout = 60;
            return dbm;
        }

        [SprocName("GetPushSubscribers")]
        public abstract List<PushSubscriber> GetPushSubscribers();

        public int UpdatePushSubscribers(int deviceID)
        {
            using (var db = GetDbManager())
            {
                return db.SetSpCommand("UpdatePushSubscribers",
                        db.Parameter("@DeviceId", deviceID)).ExecuteNonQuery();
            }
        }
    }
}
