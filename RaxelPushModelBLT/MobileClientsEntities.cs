﻿using BLToolkit.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaxelPushModelBLT
{
    public class MobileClientsEntities : DbManager {

        public MobileClientsEntities() : base("SQL", "mobileClientsEntities") { }

        public int WriteLog(string email, string pass, string token, string dopPars, string dateStart, string dateEnd, string method, string response, string requestID, string message, string tYPE)
        {
            return SetSpCommand("WriteLog",
                         Parameter("@Email", email),
                         Parameter("@Pass", pass),
                         Parameter("@Token", token),
                         Parameter("@DopPars", dopPars),
                         Parameter("@DateStart", dateStart),
                         Parameter("@DateEnd", dateEnd),
                         Parameter("@Method", method),
                         Parameter("@Response", response),
                         Parameter("@RequestID", requestID),
                         Parameter("@Message", message),
                         Parameter("@TYPE", tYPE)).ExecuteNonQuery();
        }
    }    
}
