﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLToolkit.DataAccess;
using BLToolkit.Data;


namespace RaxelPushModelBLT
{
    public abstract class FrodoPushSubscriberAccessor : DataAccessor
    {
        protected override BLToolkit.Data.DbManager CreateDbManager()
        {
            var dbm = new DbManager("SQL", "incomingEntities");
            dbm.Command.CommandTimeout = 60;
            return dbm;
        }

        [SprocName("GetFrodoPushSubscribers")]
        public abstract List<FrodoPushSubscriber> GetFrodoPushSubscribers();

        public int UpdateFrodoPushSubscribers(int? deviceID)
        {
            var param = deviceID.HasValue ? deviceID.Value : 0;
            using (var db = GetDbManager()) {
                 return db.SetSpCommand("UpdateFrodoPushSubscribers",
                         db.Parameter("@DeviceId", param)).ExecuteNonQuery();                         
                 }
        }
    }
}
