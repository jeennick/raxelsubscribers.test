﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RaxelPushServiceWcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISubsribersService
    {      
        [OperationContract]
        [WebGet]
        void ProcessPushSubscribersGet();

        [OperationContract]
        [WebInvoke(Method="POST")]
        void ProcessPushSubscribersPost();

        void ProcessPushSubscribers();
    }
}
