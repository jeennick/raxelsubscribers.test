﻿using BLToolkit.Reflection;
using RaxelPushModelBLT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var fpsa = TypeAccessor<FrodoPushSubscriberAccessor>.CreateInstance();
            var psa = TypeAccessor<PushSubscriberAccessor>.CreateInstance();

            var subscribers = psa.GetPushSubscribers();
            var frodoSubscribers = fpsa.GetFrodoPushSubscribers();

            psa.UpdatePushSubscribers(0);
            fpsa.UpdateFrodoPushSubscribers(0);

            using (var mce = new MobileClientsEntities())
            {
                mce.WriteLog(null, null, null, null, null, null, "PushNotification", null, Guid.NewGuid().ToString(), "Пушуведомление отправлено с ошибками: ", "ERROR");
            }

            var client = new SubscribersService.SubsribersServiceClient();
            client.ProcessPushSubscribersGet();
        }
    }
}
