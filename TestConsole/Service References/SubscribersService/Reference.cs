﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestConsole.SubscribersService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SubscribersService.ISubsribersService")]
    public interface ISubsribersService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubsribersService/ProcessPushSubscribersGet", ReplyAction="http://tempuri.org/ISubsribersService/ProcessPushSubscribersGetResponse")]
        void ProcessPushSubscribersGet();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubsribersService/ProcessPushSubscribersGet", ReplyAction="http://tempuri.org/ISubsribersService/ProcessPushSubscribersGetResponse")]
        System.Threading.Tasks.Task ProcessPushSubscribersGetAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubsribersService/ProcessPushSubscribersPost", ReplyAction="http://tempuri.org/ISubsribersService/ProcessPushSubscribersPostResponse")]
        void ProcessPushSubscribersPost();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubsribersService/ProcessPushSubscribersPost", ReplyAction="http://tempuri.org/ISubsribersService/ProcessPushSubscribersPostResponse")]
        System.Threading.Tasks.Task ProcessPushSubscribersPostAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISubsribersServiceChannel : TestConsole.SubscribersService.ISubsribersService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SubsribersServiceClient : System.ServiceModel.ClientBase<TestConsole.SubscribersService.ISubsribersService>, TestConsole.SubscribersService.ISubsribersService {
        
        public SubsribersServiceClient() {
        }
        
        public SubsribersServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SubsribersServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SubsribersServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SubsribersServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void ProcessPushSubscribersGet() {
            base.Channel.ProcessPushSubscribersGet();
        }
        
        public System.Threading.Tasks.Task ProcessPushSubscribersGetAsync() {
            return base.Channel.ProcessPushSubscribersGetAsync();
        }
        
        public void ProcessPushSubscribersPost() {
            base.Channel.ProcessPushSubscribersPost();
        }
        
        public System.Threading.Tasks.Task ProcessPushSubscribersPostAsync() {
            return base.Channel.ProcessPushSubscribersPostAsync();
        }
    }
}
