﻿using G21;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using log4net;

namespace RaxelWindowsService
{
    public partial class Service1 : ServiceBase
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(Service1));
        System.Timers.Timer timer = new System.Timers.Timer();

        string addressWebService = string.Empty;
        int Period = 0;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                log.Debug("RaxelWindowsService has started.");
            }
            catch (Exception e)
            {
                log.Fatal(e.ToString());
                Stop();
            }
            GetParams(out addressWebService, out Period);
            timer_Elapsed(null, null);

            timer.Interval = 1000 * Period;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            log.Debug("RaxelWindowsService has stopped.");
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs ee)
        {
            Exception e1;
            List<Rec> recs = CDataBase.GetDeviceIDs(@"Data Source=(local)\SQLEXPRESS;Initial Catalog=G2database;User ID=sa;Password=112358", out e1);
            if (e1 != null)
            {
                log.Fatal(e1.ToString());
                Stop();
            }
            if (recs.Count > 0) log.Info("The call of web-service for " + recs.Count.ToString() + " record(s).");
            try
            {
                foreach (Rec r in recs)
                {
                    // Вызываем веб метод для каждой записи r
                    //sr.Beginpushme(r.DeviceID.ToString(), null, null);                    
                    var request = (HttpWebRequest)WebRequest.Create(addressWebService + "/api/pushme?deviceid=" + r.DeviceID.ToString());
                    request.GetResponse();
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
                //Stop();
            }
        }

        void GetParams(out string addressWebService, out int period)
        {
            addressWebService = ConfigurationSettings.AppSettings["address"];
            period = int.Parse(ConfigurationSettings.AppSettings["interval"]);
        }
    }
}
