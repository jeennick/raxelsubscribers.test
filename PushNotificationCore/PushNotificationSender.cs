﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;
namespace PushNotificationCore
{
    public class PushNotificationSender
    {

        #region private fields

        string _googleCloudMessagingApiUrl = "";
        string _googleCloudMessagingApiKey = "";
        string _certificatePath = "";
        string _certificatePassword = "";

        #endregion

        public PushNotificationSender()
        {
            _googleCloudMessagingApiUrl = "https://android.googleapis.com/gcm/send";
            _googleCloudMessagingApiKey = string.Format("key={0}", ConfigurationManager.AppSettings["GoogleCloudMessagingApiKey"]);

            _certificatePath = ConfigurationManager.AppSettings["CertificatePath"];
            _certificatePassword = ConfigurationManager.AppSettings["CertificatePassword"];
        }

        /// <summary>
        /// Send notification to user
        /// </summarypublic
        /// <param name="deviceToken">GCM or Apple device token</param>
        /// <param name="message">The message</param>
        /// <exception cref="System.ArgumentException">Wrong device token</exception>
        /// <exception cref="System.ArgumentNullException">The message format exception</exception>
        /// <exception cref="System.Net.WebException">Can't connect to api server or wrong api key</exception>
         void SendNotification(string deviceToken, string message)
        {
            SendNotification(deviceToken, message, _certificatePath);
        }

        public void SendNotification(string deviceToken, string message, string certFilePath)
        {
            bool sandbox = false; // Set to false for production

            if (string.IsNullOrEmpty(deviceToken) || (deviceToken.Length < 64))
                throw new ArgumentException("Device token format exception! Token should be 64 or 162 length");

            if (string.IsNullOrEmpty(message))
                throw new ArgumentNullException("Message can't be null or empty");

            if (deviceToken.Length == 64)
            {
                //ios
                var pushNotification = new PushNotification(sandbox, certFilePath, _certificatePassword);

                var payload = new NotificationPayload(deviceToken, message);
                var payloadList = new List<NotificationPayload>();
                payloadList.Add(payload);

                pushNotification.SendToApple(payloadList);
            }
            else
            {
                //android
                WebClient wc = new WebClient();
                wc.Headers.Clear();
                wc.Headers.Add("Authorization", _googleCloudMessagingApiKey);
                wc.Headers.Add("Content-Type", "application/json");

                var request = new
                {
                    registration_ids = new string[] { deviceToken },
                    data = new
                    {
                        message = new
                        {
                            contentTitle = "Title",
                            contentText = message,
                        }
                    }
                };

                wc.UploadString(_googleCloudMessagingApiUrl, Newtonsoft.Json.JsonConvert.SerializeObject(request));
            }
        }
    }
}
