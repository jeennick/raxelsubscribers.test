﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RaxelPushDataModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class incomingEntities : DbContext
    {
        public incomingEntities()
            : base("name=incomingEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
    
        public virtual ObjectResult<GetPushSubscribers_Result> GetPushSubscribers()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetPushSubscribers_Result>("GetPushSubscribers");
        }
    
        public virtual int UpdatePushSubscribers(Nullable<int> deviceId)
        {
            var deviceIdParameter = deviceId.HasValue ?
                new ObjectParameter("DeviceId", deviceId) :
                new ObjectParameter("DeviceId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdatePushSubscribers", deviceIdParameter);
        }
    
        public virtual ObjectResult<GetFrodoPushSubscribers_Result> GetFrodoPushSubscribers()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetFrodoPushSubscribers_Result>("GetFrodoPushSubscribers");
        }
    
        public virtual int UpdateFrodoPushSubscribers(Nullable<int> deviceId)
        {
            var deviceIdParameter = deviceId.HasValue ?
                new ObjectParameter("DeviceId", deviceId) :
                new ObjectParameter("DeviceId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateFrodoPushSubscribers", deviceIdParameter);
        }
    }
}
