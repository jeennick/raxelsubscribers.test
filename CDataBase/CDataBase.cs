﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace G21
{
    public class CDataBase
    {
        public static List<Rec> GetDeviceIDs(string sqlStringConnection, out Exception ex)
        {
            ex = null;
            List<Rec> recs = new List<Rec>();
            try
            {
                SqlConnection thisConnection = new SqlConnection(sqlStringConnection);
                thisConnection.Open();
                SqlCommand thisCommand = thisConnection.CreateCommand();

                thisCommand.CommandText = "select distinct mu.MobileDeviceUID from PushMessageSubscriber pms "+
                                            "inner join device d on pms.DeviceID = d.id "+
                                            "inner join [MobileClients].[dbo].[MobileUser] mu on d.name = mu.device_id "+
                                            "where WasPushed=0 and MobileDeviceUID is not null ";
                SqlDataReader thisReader = thisCommand.ExecuteReader();                
                while (thisReader.Read())
                {
                    int DeviceID = Convert.ToInt32(thisReader["DeviceID"]);
                    DateTime DT = Convert.ToDateTime(thisReader["DTPush"]);
                    recs.Add(new Rec(DeviceID, DT));
                }
                thisReader.Close();

                foreach (Rec r in recs)
                {
                    thisCommand.CommandText = "update PushMessageSubscriber set WasPushed=1 where DeviceID=" + r.DeviceID.ToString();
                    thisCommand.ExecuteNonQuery();
                }

                thisConnection.Close();
            }
            catch (Exception e) { ex = e; }
            return recs;
        }
    }
}
