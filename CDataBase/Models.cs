﻿using System;

namespace G21
{
    public class Rec
    {
        public int DeviceID;
        public DateTime DT;
        public Rec(int deviceID, DateTime dt)
        {
            DeviceID = deviceID;
            DT = dt;
        }
    }
}
